create schema if not exists omniva;

create table if not exists email_message (
    id uuid not null constraint email_message_pkey primary key,
    receiver varchar(255) not null,
    subject varchar(255) not null,
    message text,
    send_date_time timestamp,
    status varchar(255) not null default 'NEW'
);
