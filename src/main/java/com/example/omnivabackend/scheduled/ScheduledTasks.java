package com.example.omnivabackend.scheduled;

import com.example.omnivabackend.config.properties.OmnivaProperties;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.TimeZone;

@Slf4j
@Component
@RequiredArgsConstructor
public class ScheduledTasks {

    private final OmnivaProperties omnivaProperties;
    private final TaskScheduler taskScheduler;
    private final EmailScheduledServiceTask emailScheduledServiceTask;

    @PostConstruct
    public void scheduleTasks() {
        if (omnivaProperties.isEnabledCronJobs()) {
            taskScheduler.schedule(emailScheduledServiceTask, getCronTriggerAtLatvianTimeZone(omnivaProperties.getEmailSender().getEmailSenderTaskCron()));
        } else {
            log.warn("All cron jobs disabled");
        }
    }

    private CronTrigger getCronTriggerAtLatvianTimeZone(final CronTrigger sourceTrigger) {
        return new CronTrigger(sourceTrigger.getExpression(), TimeZone.getTimeZone(ZoneId.of("Europe/Riga")));
    }

}
