package com.example.omnivabackend.scheduled;

import com.example.omnivabackend.service.api.EmailScheduledService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
@RequiredArgsConstructor
public class EmailScheduledServiceTask implements Runnable {

    private final EmailScheduledService emailScheduledService;

    @Override
    public void run() {
        log.info("Started sending emails {}", LocalDateTime.now());
        emailScheduledService.sendEmails();
        log.info("Finished sending emails {}", LocalDateTime.now());
    }

}
