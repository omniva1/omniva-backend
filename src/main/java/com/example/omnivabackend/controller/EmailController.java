package com.example.omnivabackend.controller;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.dto.EmailMessageDto;
import com.example.omnivabackend.error.AppException;
import com.example.omnivabackend.service.api.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Transactional
@CrossOrigin
@RequestMapping("/api/email")
public class EmailController {

    private final EmailService emailService;

    @GetMapping("/get_emails")
    public List<EmailMessage> getAllEmails() {
        log.info("Searching for all emails");
        List<EmailMessage> emails = emailService.getAllEmails();
        log.info("Found emails: {}", emails.size());
        return emails;
    }

    @PostMapping("/add_email")
    public EmailMessageDto addEmail(@RequestBody EmailMessageDto dto) throws AppException {
        log.info("Adding email message {}", dto);
        dto.validate();

        EmailMessage emailMessage = dto.dtoToEmail(dto);
        EmailMessage savedEmailMessage = emailService.addEmail(emailMessage);
        EmailMessageDto savedEmailDto = dto.emailToDto(savedEmailMessage);

        log.info("Added email {}", savedEmailDto);
        return savedEmailDto;
    }

}
