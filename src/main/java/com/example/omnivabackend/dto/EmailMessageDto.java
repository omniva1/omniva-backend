package com.example.omnivabackend.dto;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.error.AppException;
import com.example.omnivabackend.error.ErrorCodeType;
import com.example.omnivabackend.util.Utils;
import lombok.Data;

import java.util.Objects;

@Data
public class EmailMessageDto {

    private String receiver;
    private String subject;
    private String message;

    public void validate() throws AppException {
        beautify();

        if (Objects.isNull(receiver)) {
            throw new AppException(ErrorCodeType.EMPTY_RECEIVER_ADDRESS);
        }

        if (Objects.isNull(subject)) {
            throw new AppException(ErrorCodeType.EMPTY_SUBJECT);
        }

    }

    private void beautify() {
        if (Utils.isNullOrBlank(receiver)) {
            receiver = null;
        }

        if (Utils.isNullOrBlank(subject)) {
            subject = null;
        }
    }

    public EmailMessageDto emailToDto(EmailMessage emailMessage) {
        var dto = new EmailMessageDto();
        dto.setMessage(emailMessage.getMessage());
        dto.setReceiver(emailMessage.getReceiver());
        dto.setSubject(emailMessage.getSubject());
        return dto;
    }

    public EmailMessage dtoToEmail(EmailMessageDto dto) {
        var email = new EmailMessage();
        email.setReceiver(dto.getReceiver());
        email.setSubject(dto.getSubject());
        email.setMessage(dto.getMessage());
        return email;
    }
}
