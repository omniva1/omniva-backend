package com.example.omnivabackend.repository;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.domain.types.EmailStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.UUID;

public interface EmailRepository extends JpaRepository<EmailMessage, UUID>, JpaSpecificationExecutor<EmailMessage> {

    List<EmailMessage> findAllByStatus(EmailStatus status);
}
