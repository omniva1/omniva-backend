package com.example.omnivabackend.util;

import lombok.experimental.UtilityClass;

import java.util.Objects;

@UtilityClass
public class Utils {

    public static boolean isNullOrBlank(String string) {
        if (Objects.isNull(string)) {
            return true;
        } else {
            String tmp = string.trim();
            return tmp.isBlank();
        }
    }

}
