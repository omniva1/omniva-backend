package com.example.omnivabackend.domain;

import com.example.omnivabackend.domain.types.EmailStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@Setter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
public class EmailMessage {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    @NotNull
    private String receiver;
    @NotNull
    private String subject;
    private String message;
    private OffsetDateTime sendDateTime;
    @NotNull
    @Column(columnDefinition = "varchar(255) default 'NEW'")
    @Enumerated(EnumType.STRING)
    private EmailStatus status;

}
