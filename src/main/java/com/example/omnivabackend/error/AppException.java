package com.example.omnivabackend.error;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class AppException extends Exception {

    private ErrorCodeType errorCode;
    private String value;

    public AppException(ErrorCodeType errorCode) {
        this(errorCode, null);
    }

    public AppException(ErrorCodeType errorCode, Throwable t) {
        super(errorCode.toString(), t);
        this.errorCode = errorCode;
    }


}
