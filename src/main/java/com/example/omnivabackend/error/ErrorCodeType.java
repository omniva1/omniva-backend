package com.example.omnivabackend.error;

public enum ErrorCodeType {

    EMPTY_RECEIVER_ADDRESS,
    EMPTY_SUBJECT
}
