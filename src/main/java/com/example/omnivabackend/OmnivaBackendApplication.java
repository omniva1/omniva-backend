package com.example.omnivabackend;

import com.example.omnivabackend.config.properties.OmnivaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties({OmnivaProperties.class})
public class OmnivaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmnivaBackendApplication.class, args);
    }

}
