package com.example.omnivabackend.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.support.CronTrigger;

@Data
@NoArgsConstructor
@ConfigurationProperties(prefix = "omniva")
public class OmnivaProperties {

    private boolean enabledCronJobs = true;

    private EmailSenderProperties emailSender = new EmailSenderProperties();

    @Data
    @NoArgsConstructor
    public static class EmailSenderProperties {
        private CronTrigger emailSenderTaskCron;
    }
}
