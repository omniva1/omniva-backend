package com.example.omnivabackend.service.api;

public interface EmailScheduledService {

    void sendEmails();
}
