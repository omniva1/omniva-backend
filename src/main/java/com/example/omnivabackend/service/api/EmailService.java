package com.example.omnivabackend.service.api;

import com.example.omnivabackend.domain.EmailMessage;

import java.util.List;

public interface EmailService {

    List<EmailMessage> getAllEmails();

    EmailMessage addEmail(EmailMessage emailMessage);
}
