package com.example.omnivabackend.service.impl;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.domain.types.EmailStatus;
import com.example.omnivabackend.repository.EmailRepository;
import com.example.omnivabackend.service.api.EmailService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final EmailRepository emailRepository;

    @Override
    public List<EmailMessage> getAllEmails() {
        return emailRepository.findAll();
    }

    @Override
    public EmailMessage addEmail(EmailMessage emailMessage) {
        emailMessage.setStatus(EmailStatus.NEW);
        return emailRepository.save(emailMessage);
    }

}
