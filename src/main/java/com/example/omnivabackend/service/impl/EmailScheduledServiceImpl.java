package com.example.omnivabackend.service.impl;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.domain.types.EmailStatus;
import com.example.omnivabackend.repository.EmailRepository;
import com.example.omnivabackend.service.api.EmailScheduledService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class EmailScheduledServiceImpl implements EmailScheduledService {

    private final EmailRepository emailRepository;

    @Override
    public void sendEmails() {
        List<EmailMessage> newEmails = emailRepository.findAllByStatus(EmailStatus.NEW);
        newEmails.forEach(email -> {
            email.setStatus(EmailStatus.SEND);
            email.setSendDateTime(OffsetDateTime.now());
            emailRepository.save(email);
        });
        log.info("Sent messages count {}", newEmails.size());
    }
}
