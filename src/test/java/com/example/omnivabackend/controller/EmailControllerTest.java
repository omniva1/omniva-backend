package com.example.omnivabackend.controller;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.domain.types.EmailStatus;
import com.example.omnivabackend.dto.EmailMessageDto;
import com.example.omnivabackend.error.AppException;
import com.example.omnivabackend.error.ErrorCodeType;
import com.example.omnivabackend.service.api.EmailService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailControllerTest {

    @InjectMocks
    private EmailController emailController;

    @Mock
    private EmailService emailService;

    @Test
    void shouldReturnEmailsList() {
        EmailMessage emailMessageTest = new EmailMessage();
        emailMessageTest.setId(UUID.randomUUID());
        emailMessageTest.setReceiver("andris@gmail.com");
        emailMessageTest.setSubject("Important email");
        emailMessageTest.setMessage("This is email content");
        emailMessageTest.setStatus(EmailStatus.SEND);
        emailMessageTest.setSendDateTime(OffsetDateTime.now());

        when(emailService.getAllEmails()).thenReturn(List.of(emailMessageTest));
        var result = emailController.getAllEmails();
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getReceiver()).isEqualTo(emailMessageTest.getReceiver());
        assertThat(result.get(0).getSubject()).isEqualTo(emailMessageTest.getSubject());
        assertThat(result.get(0).getMessage()).isEqualTo(emailMessageTest.getMessage());
    }

    @Test
    void shouldAddNewEmail() throws AppException {
        EmailMessage emailMessageTest = new EmailMessage();
        emailMessageTest.setReceiver("andris@gmail.com");
        emailMessageTest.setSubject("Important email");
        emailMessageTest.setMessage("This is email content");

        EmailMessage savedEmailMessageTest = new EmailMessage();
        savedEmailMessageTest.setId(UUID.randomUUID());
        savedEmailMessageTest.setStatus(EmailStatus.NEW);
        savedEmailMessageTest.setReceiver("andris@gmail.com");
        savedEmailMessageTest.setSubject("Important email");
        savedEmailMessageTest.setMessage("This is email content");

        EmailMessageDto dto = new EmailMessageDto();
        dto.setReceiver("andris@gmail.com");
        dto.setSubject("Important email");
        dto.setMessage("This is email content");

        when(emailService.addEmail(emailMessageTest)).thenReturn(savedEmailMessageTest);
        var result = emailController.addEmail(dto);
        assertThat(result).isNotNull();
        assertThat(result.getReceiver()).isEqualTo(emailMessageTest.getReceiver());
        assertThat(result.getSubject()).isEqualTo(emailMessageTest.getSubject());
        assertThat(result.getMessage()).isEqualTo(emailMessageTest.getMessage());
    }

    @Test
    void shouldReturnEmptySubjectException() {
        EmailMessageDto dto = new EmailMessageDto();
        dto.setReceiver("andris@gmail.com");
        dto.setMessage("This is email content");

        var t = assertThrows(AppException.class, () -> emailController.addEmail(dto));
        assertThat(t.getErrorCode()).isEqualTo(ErrorCodeType.EMPTY_SUBJECT);
    }

    @Test
    void shouldReturnEmptyReceiverException() {
        EmailMessageDto dto = new EmailMessageDto();
        dto.setMessage("This is email content");
        dto.setSubject("Important email");

        var t = assertThrows(AppException.class, () -> emailController.addEmail(dto));
        assertThat(t.getErrorCode()).isEqualTo(ErrorCodeType.EMPTY_RECEIVER_ADDRESS);
    }
}
