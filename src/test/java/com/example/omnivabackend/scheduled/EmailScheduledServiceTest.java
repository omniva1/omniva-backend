package com.example.omnivabackend.scheduled;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.domain.types.EmailStatus;
import com.example.omnivabackend.repository.EmailRepository;
import com.example.omnivabackend.service.impl.EmailScheduledServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class EmailScheduledServiceTest {

    @Spy
    @InjectMocks
    private EmailScheduledServiceImpl emailScheduledService;

    @Mock
    private EmailRepository emailRepository;

    @Test
    void shouldSendEmails() {
        EmailMessage savedEmailMessageTest = new EmailMessage();
        savedEmailMessageTest.setId(UUID.randomUUID());
        savedEmailMessageTest.setStatus(EmailStatus.NEW);
        savedEmailMessageTest.setReceiver("andris@gmail.com");
        savedEmailMessageTest.setSubject("Important email");
        savedEmailMessageTest.setMessage("This is email content");

        when(emailRepository.findAllByStatus(EmailStatus.NEW)).thenReturn(List.of(savedEmailMessageTest));
        emailScheduledService.sendEmails();

        verify(emailRepository, times(1)).save(any());
    }
}
