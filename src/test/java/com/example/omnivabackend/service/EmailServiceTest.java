package com.example.omnivabackend.service;

import com.example.omnivabackend.domain.EmailMessage;
import com.example.omnivabackend.domain.types.EmailStatus;
import com.example.omnivabackend.repository.EmailRepository;
import com.example.omnivabackend.service.impl.EmailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Spy
    @InjectMocks
    private EmailServiceImpl emailService;

    @Mock
    private EmailRepository emailRepository;

    @Test
    void getAllEmails() {
        EmailMessage savedEmailMessageTest = new EmailMessage();
        savedEmailMessageTest.setId(UUID.randomUUID());
        savedEmailMessageTest.setStatus(EmailStatus.NEW);
        savedEmailMessageTest.setSendDateTime(OffsetDateTime.now());
        savedEmailMessageTest.setReceiver("andris@gmail.com");
        savedEmailMessageTest.setSubject("Important email");
        savedEmailMessageTest.setMessage("This is email content");

        when(emailRepository.findAll()).thenReturn(List.of(savedEmailMessageTest));

        List<EmailMessage> result = emailService.getAllEmails();

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getReceiver()).isEqualTo(savedEmailMessageTest.getReceiver());
        assertThat(result.get(0).getSubject()).isEqualTo(savedEmailMessageTest.getSubject());
        assertThat(result.get(0).getMessage()).isEqualTo(savedEmailMessageTest.getMessage());
    }

    @Test
    void addEmail() {
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setReceiver("andris@gmail.com");
        emailMessage.setSubject("Important email");
        emailMessage.setMessage("This is email content");

        EmailMessage savedEmailMessageTest = new EmailMessage();
        savedEmailMessageTest.setStatus(EmailStatus.NEW);
        savedEmailMessageTest.setReceiver("andris@gmail.com");
        savedEmailMessageTest.setSubject("Important email");
        savedEmailMessageTest.setMessage("This is email content");

        when(emailRepository.save(savedEmailMessageTest)).thenReturn(savedEmailMessageTest);

        EmailMessage result = emailService.addEmail(emailMessage);

        assertThat(result).isNotNull();
        assertThat(result.getReceiver()).isEqualTo(savedEmailMessageTest.getReceiver());
        assertThat(result.getSubject()).isEqualTo(savedEmailMessageTest.getSubject());
        assertThat(result.getMessage()).isEqualTo(savedEmailMessageTest.getMessage());
        assertThat(result.getSendDateTime()).isNull();
        assertThat(result.getStatus()).isEqualTo(EmailStatus.NEW);
    }
}
