# omniva-backend

## This application has been built with

- JDK 17
- Gradle 7.6
- Spring Boot 3.0.1

## How to run application

#### 1. Create postgres database in docker container

Start postgres docker container from file [docker-compose.yml](docker/docker-compose.yml)

Default port 5435, you can change it in docker-compose file

#### 2. Adjust application properties if you need in file [application.yml](src/main/resources/application.yml)

Default running port 8080

#### 3. Build and start application